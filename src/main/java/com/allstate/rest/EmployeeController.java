package com.allstate.rest;

import com.allstate.entities.Employee;
import com.allstate.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/statusapi*")
public class EmployeeController implements IEmployeeController {
    @Autowired
    private EmployeeService service;

    @Override
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus(){
        return "Employee Rest Api";
    }

    @Override
    @RequestMapping(value = "/total", method = RequestMethod.GET)
    public long getTotal(){
        return service.total();
    }

    @Override
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Employee> all(){
        return service.all();
    }

    @Override
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public Employee find(@PathVariable("id") int id){
        return service.findById(id);
    }

    @Override
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save(@RequestBody Employee employee){
        service.save(employee);
    }

    @Override
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public long update(@RequestBody Employee employee){
        return service.update(employee);
    }


}
