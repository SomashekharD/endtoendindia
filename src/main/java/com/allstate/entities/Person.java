package com.allstate.entities;


import org.springframework.data.annotation.Id;

public class Person {
    @Id
    private int id;
    private String name;
    private int age;
    private static int ageLimit = 100;

    public int getId() {
        return id;
    }

    public static int getAgeLimit() {
        return ageLimit;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public void display(){
        System.out.printf("Values are %s, %d ", this.name, this.id);
    }

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Person() {
        
    }
}
