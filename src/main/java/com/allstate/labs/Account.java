package com.allstate.labs;

public abstract class Account {
    private Double balance;
    private String name;
    private static int interestRate = 10;

    public static void setInterestRate(int intrRt) {
        Account.interestRate = intrRt;
    }

    public boolean withdraw(double amount)  {
        if (this.balance >= amount) {
            this.balance -= amount;
            return true;
        }
        return false;
    }

    public boolean withdraw()  {
        return this.withdraw(20);
    }

    public Double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public abstract void addInterest();

    // public void addInterest(){
    //     this.balance += this.balance * Account.interestRate/100;
    //     System.out.printf("Values are %s, %.2f", this.name, this.balance);
    //     System.out.println();
    // }

    public Account(Double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public void showDetails(){
        System.out.printf("Values are %s, %.2f", this.name, this.balance);
        System.out.println();
    }


}
