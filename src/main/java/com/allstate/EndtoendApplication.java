package com.allstate;
import com.allstate.di.Owner;

import org.springframework.context.ApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EndtoendApplication {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(EndtoendApplication.class, args);
		context.getBean(Owner.class).getPet().feed();
		//		SpringApplication.run(EndtoendApplication.class, args);
	}

}
